﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtosSecondHand.Models.IncomingModels
{
    public class LegalEntityRegisterModel
    {
        public string CompanyName { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyPhone { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string WebSite { get; set; }
        public string TaxNo { get; set; }
        public string TaxPlace { get; set; }
        public string DeviceType { get; set; }
        public int AddressID { get; set; }
    }
}
