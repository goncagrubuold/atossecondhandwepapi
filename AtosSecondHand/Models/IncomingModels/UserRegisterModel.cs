﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtosSecondHand.Models.IncomingModels
{
    public class UserRegisterModel
    {
        public string DevicePushToken { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NameSurname { get; set; }
        public string TcNo { get; set; }
        public string Phone { get; set; }
        public int AddressID { get; set; }
    }
}