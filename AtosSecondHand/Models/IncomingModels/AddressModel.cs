﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtosSecondHand.Models.IncomingModels
{
    public class AddressModel
    {
        public int UserIDorLegalEntityID { get; set; }
        public int? AddressID { get; set; }
        public string Token { get; set; }
        public int DistrictID { get; set; }
        public string AddressDetail { get; set; }
        public string MapsUrl { get; set; }
        public bool IsLegalEntity { get; set; }
    }
}