﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtosSecondHand.Models;
using AtosSecondHand.Models.IncomingModels;

namespace AtosSecondHand.Controllers.v1
{
    public class AddressController : ApiController
    {
        public HttpResponseMessage Get([FromUri]AddressModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                if (Incoming.IsLegalEntity == true)
                {
                    var legalEntity = db.tbl_LegalEntity.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (legalEntity == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (legalEntity.RefreshToken == Incoming.Token)
                        {
                            var data = db.tbl_Address.Where(x => x.ID == legalEntity.AddressID).Select(x => new
                            {
                                x.ID,
                                x.Address,
                                DistrictName = x.tbl_District.Name,
                                CityName = x.tbl_District.tbl_City.Name,
                                x.MapsUrl
                            }).FirstOrDefault();
                            var response = new { data, message = ReturnMessages.Success };
                            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
                else
                {
                    var user = db.tbl_User.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (user == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (user.RefreshToken == Incoming.Token)
                        {
                            var data = db.tbl_Address.Where(x => x.ID == user.AdressID).Select(x => new
                            {
                                x.ID,
                                x.Address,
                                DistrictName = x.tbl_District.Name,
                                CityName = x.tbl_District.tbl_City.Name,
                                x.MapsUrl
                            }).FirstOrDefault();
                            var response = new { data, message = ReturnMessages.Success };
                            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
            }
        }
        public HttpResponseMessage Post([FromBody]AddressModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                if (Incoming.IsLegalEntity == true)
                {
                    var legalEntity = db.tbl_LegalEntity.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (legalEntity == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (legalEntity.RefreshToken == Incoming.Token)
                        {
                            tbl_Address address = new tbl_Address();
                            address.Address = Incoming.AddressDetail;
                            address.DistrictID = Incoming.DistrictID;
                            db.tbl_Address.Add(address);
                            if (db.SaveChanges() > 0)
                            {
                                var lastID = db.tbl_Address.Max(x => x.ID);
                                var response = new { lastID, message = ReturnMessages.Success };
                                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                var response = new { message = ReturnMessages.AddressAddError };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
                else
                {
                    var user = db.tbl_User.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (user == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (user.RefreshToken == Incoming.Token)
                        {
                            tbl_Address address = new tbl_Address();
                            address.Address = Incoming.AddressDetail;
                            address.DistrictID = Incoming.DistrictID;
                            db.tbl_Address.Add(address);
                            if (db.SaveChanges() > 0)
                            {
                                var lastID = db.tbl_Address.Max(x => x.ID);
                                var response = new { lastID, message = ReturnMessages.Success };
                                return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                var response = new { message = ReturnMessages.AddressAddError };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
            }
        }
        public HttpResponseMessage Put([FromBody]AddressModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                if (Incoming.IsLegalEntity == true)
                {
                    var legalEntity = db.tbl_LegalEntity.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (legalEntity == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (legalEntity.RefreshToken == Incoming.Token)
                        {
                            var address = db.tbl_Address.Where(x => x.ID == Incoming.AddressID).FirstOrDefault();
                            if (address == null)
                            {
                                var response = new { message = ReturnMessages.DefinitionNotFound };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                address.Address = Incoming.AddressDetail;
                                address.MapsUrl = Incoming.MapsUrl;
                                address.DistrictID = Incoming.DistrictID;
                                if (db.SaveChanges() > 0)
                                {
                                    var response = new { message = ReturnMessages.Success };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                                else
                                {
                                    var response = new { message = ReturnMessages.DBError };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
                else
                {
                    var user = db.tbl_User.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (user == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (user.RefreshToken == Incoming.Token)
                        {
                            var address = db.tbl_Address.Where(x => x.ID == Incoming.AddressID).FirstOrDefault();
                            if (address == null)
                            {
                                var response = new { message = ReturnMessages.DefinitionNotFound };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                address.Address = Incoming.AddressDetail;
                                address.MapsUrl = Incoming.MapsUrl;
                                address.DistrictID = Incoming.DistrictID;
                                if (db.SaveChanges() > 0)
                                {
                                    var response = new { message = ReturnMessages.Success };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                                else
                                {
                                    var response = new { message = ReturnMessages.DBError };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
            }
        }
        public HttpResponseMessage Delete([FromBody]AddressModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                if (Incoming.IsLegalEntity==true)
                {
                    var legalEntity = db.tbl_LegalEntity.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (legalEntity == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (legalEntity.RefreshToken == Incoming.Token)
                        {

                            var address = db.tbl_Address.Where(x => x.ID == Incoming.AddressID).FirstOrDefault();
                            if (address == null)
                            {
                                var response = new { message = ReturnMessages.DefinitionNotFound };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                legalEntity.AddressID = null;
                                db.SaveChanges();
                                db.tbl_Address.Remove(address);
                                if (db.SaveChanges() > 0)
                                {
                                    var response = new { message = ReturnMessages.Success };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                                else
                                {
                                    var response = new { message = ReturnMessages.DBError };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
                else
                {
                    var user = db.tbl_User.Where(x => x.ID == Incoming.UserIDorLegalEntityID).FirstOrDefault();
                    if (user == null)
                    {
                        var response = new { message = ReturnMessages.UserNotFound };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        if (user.RefreshToken == Incoming.Token)
                        {

                            var address = db.tbl_Address.Where(x => x.ID == Incoming.AddressID).FirstOrDefault();
                            if (address == null)
                            {
                                var response = new { message = ReturnMessages.DefinitionNotFound };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                user.AdressID = null;
                                db.SaveChanges();
                                db.tbl_Address.Remove(address);
                                if (db.SaveChanges() > 0)
                                {
                                    var response = new { message = ReturnMessages.Success };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                                else
                                {
                                    var response = new { message = ReturnMessages.DBError };
                                    return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
                                }
                            }
                        }
                        else
                        {
                            var response = new { message = ReturnMessages.TokenError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, response, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
                
                

            }
        }
        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }

    }
}
