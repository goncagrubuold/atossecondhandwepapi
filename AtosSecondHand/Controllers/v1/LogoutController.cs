﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtosSecondHand.Models;

namespace AtosSecondHand.Controllers.v1
{
    public class LogoutController : ApiController
    {
        public HttpResponseMessage Post([FromBody]int UserID)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                var user = db.tbl_User.Where(x => x.ID == UserID).FirstOrDefault();
                if (user == null)
                {
                    var data = new { message = ReturnMessages.LogoutError };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    user.RefreshToken = null;
                    user.LogoutDate = DateTime.Now;
                    if (db.SaveChanges() > 0)
                    {
                        var data = new { message = ReturnMessages.LogoutSuccess };
                        return Request.CreateResponse(HttpStatusCode.OK, data, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        var data = new { message = ReturnMessages.DBError };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                }
            }
        }
        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }
    }
}
