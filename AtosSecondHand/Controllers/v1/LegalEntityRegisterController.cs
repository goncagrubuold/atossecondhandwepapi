﻿using AtosSecondHand.Models;
using AtosSecondHand.Models.IncomingModels;
using AtosSecondHand.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AtosSecondHand.Controllers.v1
{
    public class LegalEntityRegisterController : ApiController
    {
        public HttpResponseMessage Post([FromBody]LegalEntityRegisterModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                try
                {

                    MD5Creator md5 = new MD5Creator();
                    string pass = md5.MD5Hash(Incoming.Password);
                    tbl_LegalEntity legalEntity = new tbl_LegalEntity();
                    int isEmailExist = db.tbl_LegalEntity.Count(x => x.Email == Incoming.Email);
                    int isUserNameExist = db.tbl_LegalEntity.Count(x => x.UserName == Incoming.UserName);
                    if (isEmailExist > 0)
                    {
                        var data = new { message = ReturnMessages.EmailExist };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                    else if (isUserNameExist > 0)
                    {
                        var data = new { message = ReturnMessages.UserNameExist };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        legalEntity.UserName = Incoming.UserName;
                        legalEntity.CompanyName = Incoming.CompanyName;
                        legalEntity.CompanyFax = Incoming.CompanyFax;
                        legalEntity.CompanyPhone = Incoming.CompanyPhone;
                        legalEntity.AddressID = Incoming.AddressID;
                        legalEntity.AuthorizationGroupNameID = 2;//TODO klasik sirket diye bir veri ekle dbye
                        legalEntity.Email = Incoming.Email;
                        legalEntity.TaxNo = Incoming.TaxNo;
                        legalEntity.TaxPlace = Incoming.TaxPlace;
                        legalEntity.Password = pass;
                        legalEntity.LoginDate = DateTime.Now;
                        legalEntity.RegisterDate = DateTime.Now;
                        legalEntity.IsActived = true;
                        legalEntity.IsComplated = false;
                        legalEntity.IsEmailConfirmation = false;
                        legalEntity.IsTaxNoConfirmation = false;
                        string tokenn = legalEntity.ID.ToString() + DateTime.Now.ToString();
                        string token = md5.MD5Hash(tokenn);
                        db.tbl_LegalEntity.Add(legalEntity);
                        if (Incoming.DeviceType != null || Incoming.DeviceType != "")
                        {
                            if (db.SaveChanges() > 0)
                            {
                                int maxLegalEntityID = db.tbl_LegalEntity.Max(x => x.ID);
                                db.SaveChanges();
                                var data = new { NameSurname = legalEntity.CompanyName, LegalEntityID = maxLegalEntityID, Token = token };
                                return Request.CreateResponse(HttpStatusCode.OK, data, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                var data = new { message = ReturnMessages.RegisterError };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                            }
                        }
                        else
                        {
                            var data = new { message = ReturnMessages.DeviceOptionsError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                        }
                    }

                }
                catch (Exception ex)
                {
                    var data = new { message = ReturnMessages.SystemError, errorMessage = ex.Message };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                }
            }
        }
        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }
    }
}
