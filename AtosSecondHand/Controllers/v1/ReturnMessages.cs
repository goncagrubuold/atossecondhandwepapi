﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtosSecondHand.Controllers.v1
{
    public class ReturnMessages
    {
        public static string UserNameorPassWrong = "Kullanıcı adı veya şifre yanlış";
        public static string RegisterError = "Kayıt sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
        public static string SystemError = "Sistemde bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
        public static string EmailExist = "Bu eMail adresi zaten kayıtlı.";
        public static string UserNameExist = "Bu kullanıcı adı zaten kayıtlı.";
        public static string OptionSuccess = "Option başarılı";
        public static string DeviceOptionsError = "Cihaz ayarlarinda bir problem var.";
        public static string LogoutSuccess = "Çıkış başarılı";
        public static string LogoutError = "Çıkış işlemi sırarsında bir hata oluştu";
        public static string UserNotFound = "Kullanıcı bulunamadı";
        public static string Success = "İşlem başarılı";
        public static string TokenError = "Token gerçek değil yada yanlış.";
        public static string DefinitionNotFound = "İlgili kayıt bulunamadı.";
        public static string DBError = "Veritabani islemi yapilirken bir hata olustu.";
        public static string DocumentTypeAddEror = "Belge turu eklenirken bir hata oluştu.";
        public static string CarTypeAddEror = "Arac tipi eklenirken bir hata oluştu.";
        public static string WheelProcessAddEror = "Arac lastigi eklenirken bir hata oluştu.";
        public static string CarStatusAddEror = "Arac durumu eklenirken bir hata oluştu.";
        public static string CarSegmentAddEror = "Arac segmenti eklenirken bir hata oluştu.";
        public static string AddressAddError = "Adres eklenirken bir hata oluştu.";
        public static string CompanyAddEror = "Firma eklenirken bir hata oluştu.";
        public static string CarAddEror = "Arac eklenirken bir hata oluştu.";
        public static string DocTypeEror = "Dokuman tipi hatali.";
        public static string DocBigEror = "Dokuman cok buyuk max 5 mb.";
        public static string AuthorizationGroupNameAddEror = "Kullanici yetki grubu eklenirken bir hata olustu";
        public static string OldPasswordDosNotMatch = "Eski sifre dogru degil.";
        public static string IsActivedFalse = "Askiya alinmis firma yada onaylanmamis islem";
        public static string AuthChangeError = "Grup ismi degisti ancak yetkilendirmeler degistirilirken hata olustu";
        public static string DataIsAlreadyExist = "Bu veri zaten kayitli";
    }
}