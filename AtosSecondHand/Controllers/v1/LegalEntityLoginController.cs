﻿using AtosSecondHand.Models;
using AtosSecondHand.Models.IncomingModels;
using AtosSecondHand.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AtosSecondHand.Controllers.v1
{
    public class LegalEntityLoginController : ApiController
    {
        public HttpResponseMessage Post([FromBody]LoginModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {

                MD5Creator md5 = new MD5Creator();
                string pass = md5.MD5Hash(Incoming.Password);
                tbl_LegalEntity legalEntity = db.tbl_LegalEntity.Where(x => x.UserName == Incoming.UserName && x.Password == pass).FirstOrDefault();
                if (legalEntity == null)
                {
                    var data = new { message = ReturnMessages.UserNameorPassWrong };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    if (legalEntity.IsActived == false)
                    {
                        var data = new { message = ReturnMessages.IsActivedFalse };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        string tokenn = legalEntity.ID.ToString() + DateTime.Now.ToString();
                        string token = md5.MD5Hash(tokenn);
                        legalEntity.RefreshToken = token;
                        var authGroupName = legalEntity.tbl_AuthorizationGroupName.Name;
                        var grantList = db.tbl_AuthorizationGroup.Where(x => x.AuthorizationGroupNameID == legalEntity.AuthorizationGroupNameID).Select(x => new
                        {
                            x.AuthorizationID,
                            x.tbl_Authorization.Name,
                        }).ToList();
                        if (db.SaveChanges() > 0)
                        {
                            var data = new { UserName = legalEntity.UserName, UserID = legalEntity.ID, Token = token, authGroupName, grantList };
                            return Request.CreateResponse(HttpStatusCode.OK, data, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var data = new { message = ReturnMessages.DBError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                        }
                    }

                }
            }
        }

        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }
    }
}
