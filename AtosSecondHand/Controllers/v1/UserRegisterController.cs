﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtosSecondHand.Models;
using AtosSecondHand.Models.IncomingModels;
using AtosSecondHand.Tools;

namespace AtosSecondHand.Controllers.v1
{
    public class UserRegisterController : ApiController
    {
        public HttpResponseMessage Post([FromBody]UserRegisterModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {
                try
                {
                    if (Incoming.DeviceToken != null && Incoming.DeviceType != null)
                    {
                        MD5Creator md5 = new MD5Creator();
                        string pass = md5.MD5Hash(Incoming.Password);
                        tbl_User user = new tbl_User();
                        int isEmailExist = db.tbl_User.Count(x => x.Email == Incoming.Email);
                        int isUserNameExist = db.tbl_User.Count(x => x.UserName == Incoming.UserName);
                        if (isEmailExist > 0)
                        {
                            var data = new { message = ReturnMessages.EmailExist };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                        }
                        else if (isUserNameExist > 0)
                        {
                            var data = new { message = ReturnMessages.UserNameExist };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            user.UserName = Incoming.UserName;
                            user.AdressID = Incoming.AddressID;
                            user.AuthorizationGroupNameID = 1;//TODO klasik user diye bir veri ekle dbye
                            user.IsEmailConfirmation = false;
                            user.IsPhoneConfirmation = false;
                            user.SettingEmailSend = true;
                            user.SettingPushSed = true;
                            user.SettingSmsSend = true;
                            user.Email = Incoming.Email;
                            user.Phone = Incoming.Phone;
                            user.TcNo = Incoming.TcNo;
                            user.Password = pass;
                            user.IsActived = true;
                            user.RegisterDate = DateTime.Now;
                            user.IsComplated = false;
                            user.NameSurname = Incoming.NameSurname;
                            string tokenn = user.ID.ToString() + DateTime.Now.ToString();
                            string token = md5.MD5Hash(tokenn);
                            user.PushToken = Incoming.DevicePushToken;
                            user.DeviceToken = Incoming.DeviceToken;
                            user.DeviceType = Incoming.DeviceType;
                            user.RefreshToken = token;
                            user.LoginDate = DateTime.Now;
                            db.tbl_User.Add(user);
                            if (db.SaveChanges() > 0)
                            {
                                int maxUserID = db.tbl_User.Max(x => x.ID);
                                db.SaveChanges();
                                var data = new { NameSurname = user.NameSurname,  UserID = maxUserID, Token = token };
                                return Request.CreateResponse(HttpStatusCode.OK, data, Configuration.Formatters.JsonFormatter);
                            }
                            else
                            {
                                var data = new { message = ReturnMessages.RegisterError };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                            }
                        }
                    }
                    else
                    {
                        var data = new { message = ReturnMessages.DeviceOptionsError };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                }
                catch (Exception ex)
                {
                    var data = new { message = ReturnMessages.SystemError, errorMessage = ex.Message };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                }
            }
        }
        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }
    }
}
