﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtosSecondHand.Models;
using AtosSecondHand.Models.IncomingModels;
using AtosSecondHand.Tools;

namespace AtosSecondHand.Controllers.v1
{
    public class UserLoginController : ApiController
    {
        public HttpResponseMessage Post([FromBody]LoginModel Incoming)
        {
            using (atossalesEntities db = new atossalesEntities())
            {

                MD5Creator md5 = new MD5Creator();
                string pass = md5.MD5Hash(Incoming.Password);
                tbl_User user = db.tbl_User.Where(x => x.UserName == Incoming.UserName && x.Password == pass).FirstOrDefault();
                if (user == null)
                {
                    var data = new { message = ReturnMessages.UserNameorPassWrong };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    if (user.IsActived == false)
                    {
                        var data = new { message = ReturnMessages.IsActivedFalse };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        string tokenn = user.ID.ToString() + DateTime.Now.ToString();
                        string token = md5.MD5Hash(tokenn);
                        user.RefreshToken = token;
                        var authGroupName = user.tbl_AuthorizationGroupName.Name;
                        var grantList = db.tbl_AuthorizationGroup.Where(x => x.AuthorizationGroupNameID == user.AuthorizationGroupNameID).Select(x => new
                        {
                            x.AuthorizationID,
                            x.tbl_Authorization.Name,

                        }).ToList();
                        if (db.SaveChanges() > 0)
                        {
                            var data = new { UserName = user.UserName, UserID = user.ID, Token = token, authGroupName, grantList };
                            return Request.CreateResponse(HttpStatusCode.OK, data, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            var data = new { message = ReturnMessages.DBError };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, data, Configuration.Formatters.JsonFormatter);
                        }
                    }

                }
            }
        }

        public HttpResponseMessage Options()
        {
            var response = new { message = ReturnMessages.OptionSuccess };
            return Request.CreateResponse(HttpStatusCode.OK, response, Configuration.Formatters.JsonFormatter);
        }
    }
}
